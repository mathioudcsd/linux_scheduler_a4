#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/d_params.h>
#include <asm-generic/errno-base.h>
#include <asm/current.h>
extern int total_demand;
extern struct task_struct *head_with_demand;

struct task_struct *search_process(int target_pid){
	struct task_struct *p;
	struct task_struct *target=NULL;
	for_each_process(p){
		if(p->pid == target_pid){
			target=p;
			return target;
		}
		else continue;
	}
	return NULL;
}

int insert_with_demand(struct task_struct* new, int demand){
	struct task_struct* head=head_with_demand;
	struct task_struct* ptr;
	struct task_struct* prev;
	if(head==NULL){
		head=new;
		new->next_with_demand=NULL;
	}
	else{
		ptr=head;
		while(demand>ptr->demand){
			prev=ptr;
			ptr=ptr->next_with_demand;
		}
		prev->next_with_demand=new;
		new->next_with_demand=ptr;
	}
}

asmlinkage long sys_set_demand(int pid, int demand){
	printk("mathioud_AM2720 - sys_SET_demand\n");
	struct list_head *p;
	struct task_struct *task;
	struct task_struct *victim;
	struct task_struct *buff;
	struct task_struct *curr_task = get_current();
	int curr_pid = curr_task->pid;

	if(pid<-1){
		printk("Invalid pid(set_demand)\n");
		return EINVAL;
	}
	if(pid==curr_pid || pid==-1){
		if(demand+total_demand<=100){
			curr_task->demand = demand;
			curr_task->active=1;
			total_demand+=demand;
			
			/* insert in with_demand list */
			insert_with_demand(curr_task, demand);
			printk("PARENT: TOTAL DEMAND = %d\n",total_demand);
			printk("PARENT: demand = %d\n", curr_task->demand);
		}
		else{
			printk("cpu demand time exceeds 100.\n");
			printk("Final total_demand=%d\n",total_demand);
			return EINVAL;
		}
	} 
	else{
		victim = search_process(pid);
		if(victim==NULL)
			return EINVAL;
		else{
			if(victim->parent->pid == curr_pid){
				if(demand+total_demand<=100){
					victim->demand=demand;
					victim->active=1;
					total_demand+=demand;
					/* insert in with_demand list */
					insert_with_demand(victim, demand);
					printk("CHILD: TOTAL DEMAND = %d\n", total_demand);
					printk("CHILD: demand =%d\n", victim->demand);
					return 0;	
				}
				else{
					printk("The DEMAND for child, EXCEEDS accepted range, try smaller demand\n");
					printk("cpu demand time exceeds 100.\n",total_demand);
					return EINVAL;
				}
			}
			else{
				printk("No child with pid=%d!\n", pid);
				return EINVAL;
			}
		}
	}		
}

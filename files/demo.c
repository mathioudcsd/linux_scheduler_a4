#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#define SET 341
#define GET 342

struct d_params{
	int demand;
};

static int *gvar;

void childProcess(){
	*gvar = getpid();
	sleep(4);
}

void parentProcess(){
	struct d_params *kouvas=malloc(sizeof(struct d_params));
	sleep(2);
	syscall(SET, *gvar, 14);
	syscall(SET, -1, 33 );
	syscall(SET, 900, 8);
	syscall(GET, -1, kouvas);
	printf("DAD:My demand: %d\n", kouvas->demand);
	syscall(GET, *gvar, kouvas);
	printf("DAD:kid's demand:%d\n", kouvas->demand);
	munmap(gvar, sizeof *gvar);
}

int main(){
	gvar = mmap(NULL, sizeof *gvar, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	if(fork()==0) {/* child */
		childProcess();
	}
	else
		parentProcess();
	return 0;
}

#### HY345 - Fotis Mathioudakis am:2720 ####

Prosthetoume ta pedia "active" kai "next_with_demand"
sto Struct task_struct.

Ta idia pedia ta exoume orisei sto /init/main.c, kai ta xrisimopoioume me extern.

Phgainoume sto sched.c, line 3992, kai sto shmeio pou
apofasizetai poio tha einai to epomeno task gia to
context switch, parathetoume to diko mas algorithmo,
o opoios, AN yparxei task me orismeno to pedio "demand"
epilegoume to mikrotero se demand time.

Kanoume kai to printk me to pid tou task.

Oson afora to set_demand, to allaksa ligo xreiastike na dhmiourgisoume
mia nea sunartisi "insert_with_demand()", h opoia ftiaxnei mia
lista me structs ta opoia exoun orismeno demand, se auksousa seira
kai kratame ena deikti panta sto prwto task_struct (demand = min)

Me auton ton tropo apofeugoume tin prospelasi olwn twn diergasiwn gia na vroume
ta tasks ta opoia exoun orismeno to pedio demand.

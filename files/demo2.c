#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#define SET 341
#define GET 342

struct d_params{
	int demand;
};

static int *gvar;

void childProcess(){
	sleep(1);
	struct d_params *kouti =malloc(sizeof(struct d_params));
	kouti->demand=-99;
	syscall(GET, *gvar, kouti);
	printf("KID:my dad's demand:%d\n", kouti->demand);
}

void parentProcess(){
	syscall(SET, -1, 33);
	*gvar = getpid();
	sleep(2);
	munmap(gvar, sizeof *gvar);
}

int main(){
	gvar = mmap(NULL, sizeof *gvar, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	if(fork()==0) {/* child */
		childProcess();
	}
	else
		parentProcess();
	return 0;
}
